# README #

The classification of pathalogic surveys is a time consuming endevour. This program 
uses model obtained by machine learning to classify such surveys between benign 
and malignant types of cancer. The program is written in such a way that anybody 
can use it. Also the results are meant to be easily interpreted. 
  
This classifyer ONLY works for the Wisconsin breast cell data set WITHOUT ID code!!!

### What is this repository for? ###

* Quick summary
* Version

### How do I get set up? ###

First clone the repository to your local computer
* Make sure you have a jvm installed  
thats it!

Next your open the folder which you just cloned to your computer and open a terminal  
type in:  
`java -jar wrapper.jar`  
  
This will give you the help page of the program  
For classifying a file you use flag `-f` followed by the `.arff` file you want to classify  
`java -jar wrapper.jar -f Test.arff`  
  
For classifying a single instance you can parse flag `-i`  
Make sure to use the following format:  
`"8,2,7,2,9,7,8,2,?"`
  

### Who do I talk to? ###

You talk to me if you have a problem with the program, I'll try to help you out.  

e-mail: j.gray\@st.hanze.nl