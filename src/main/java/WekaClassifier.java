import weka.classifiers.meta.CostSensitiveClassifier;
import weka.core.Instance;
import weka.core.Instances;

/** this Class classifies instances using the model
 *  i provided it
 */

public class WekaClassifier {
    // make these variables available for the class
    private final String filename;
    private CostSensitiveClassifier classifier;
    private Instances instances = null;

    // Set filename an run classifier
    public WekaClassifier(String filename) {
        this.filename = filename;
        run();
    }

    private void run() {
        // try to run
        try {
            // create new instance handler object an parse it the filename
            InstanceHandler instanceHandler = new InstanceHandler(this.filename);
            // this returns an instances object
            this.instances = instanceHandler.getInstances();
            // build the classifier, saved to class variable
            buildClassifier();
            // Classify the instances
            classifyInstances();
            // but cant hide, i will find you! Exception >:-(
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method reads in the magic model, created by moi
     */
    private void buildClassifier() {
        try {
            // Read model and save as cost sensitive classifier
            CostSensitiveClassifier cSnB = (CostSensitiveClassifier) weka.core.SerializationHelper
                    .read("data/cs_nb.model");
            // store in class variable
            this.classifier = cSnB;
        } catch (Exception e) {
            // print error if there is one, lets be honest. there will be one.
            e.printStackTrace();
        }
    }

    /**
     * Classify instances
     *
     * @return
     */
    public Instances classifyInstances() {
        // Create new instances object for the freshly labeled instances
        Instances labeled = new Instances(this.instances);

        // for all instances in instances
        for (int i = 0; i < this.instances.numInstances(); i++) {
            // classification is stored in label
            double label = 0;
            // if the class is missing (?)
            if (this.instances.instance(i).classIsMissing()) {
                try {
                    // Try to classify
                    label = this.classifier.classifyInstance(this.instances.instance(i));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Cast the label back into a string and replace it with a human readable value
                String predstring = this.instances.classAttribute().value((int) label)
                        .replace("4", "Malignant").replace("2", "Benign");
                // Tell the user at what line the classification has taken place
                System.out.println("Classified instance at instance: " + i + ":");
                System.out.println(predstring);
                // print instance without classification
                System.out.println(this.instances.instance(i));
                // save in the labeled instances
                labeled.instance(i).setClassValue(label);
                // print instance with classification
                System.out.println(labeled.instance(i)+"\n");

            }
        }
        // return labeled instances
        return labeled;
    }
}
