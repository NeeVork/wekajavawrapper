import org.apache.commons.cli.*;

public class ArgumentParser {
    private Options options = new Options();
    private final String[] arguments;
    private CommandLine cmd;

    /**
     * This is a Class that gets arguments from the commandline
     * for now the used parameters are:
     * -h --help
     * -f --file [filename.arff]
     * -i --instance instance.arff
     *
     * @param //args
     * @throws //IllegalAccessException
     */

    public ArgumentParser(String[] args){
        this.arguments = args;
        //Run the parser
        run();
    }

    /**
     * This is a small method that runs all the machinery in the class
     */
    private void run(){
        createArguments();
        parseToArguments();
    }

    /**
     * This is a method that returns a CommandLine Object with all
     * options an values
     *
     * @return CommandLine
     */

    public CommandLine parseOptions() {
        // This gives back everything the developer (me) wants to use
        return this.cmd;
    }

    /**
     * Method that gets and stores values from the commandline
     * -f file
     * -i instance
     * -h help
     */

    private void createArguments() {
        options.addOption(Option.builder("f") // Add argument for file
                .longOpt("file")
                .argName("fileName")
                .hasArg()
                .desc("Enter filename and path")
                .build()).addOption(Option.builder("i")
                .longOpt("instance") // Add argument for instance
                .argName("SingleInstance")
                .hasArg()
                .desc("Enter a new instance surounded by double quotes " +
                        "Make sure the string is 9 digits long and " +
                        "seperated only by comma " +
                        "make sure the last attribute is a question mark.\n" +
                        "example: \"3,6,2,3,8,9,6,4,?\"")
                .build()).addOption("h", false, // And one for the help flag
                "Help page");
    }

    /**
     * This method creates a CommandLine object
     * If illegal arguments are given, an is thrown
     */

    private void parseToArguments(){
        // Parsing arguments isn't always so straight forward. This method tries to catch any errors
        // and return a useful error to the user.
        try {
            CommandLineParser parser = new DefaultParser();
            this.cmd = parser.parse(this.options, this.arguments);
        } catch (Exception e) {
            this.printHelp();
            e.printStackTrace();

        }
    }

    /** Prints help, what more do you need to know?
     * (Serious question)*/
    public void printHelp() {
        // this prints help and some info about the helper.
        System.out.println("author: James Gray \n" +
                "email: j.gray@st.hanze.nl\n" +
                "This is a program to determine pathological surveys of breast cancer\n" +
                "between benign and malignant\n\n");
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("WekaJavaWrapper\n", options);
    }
}
