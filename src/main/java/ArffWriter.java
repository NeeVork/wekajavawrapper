import java.io.FileWriter;

public class ArffWriter {
    /**
     * This class makes a temporary file for single line classification
     * It writes boilerplate arff material to the file and appends the user input
     */
    public ArffWriter(String line){
        run(line);
    }

    static void run(String line){
        fileCreator(line);
    }

    private static void fileCreator(String line){
        String header = "@relation breast_cell_training-weka.filters.unsupervised.attribute.Remove-R1-2-weka.filters.unsupervised.attribute.NumericToNominal-R10-weka.filters.unsupervised.attribute.Remove-R9\n" +
                "\n" +
                "@attribute Cl.thickness numeric\n" +
                "@attribute Cell.size numeric\n" +
                "@attribute Cell.shape numeric\n" +
                "@attribute Marg.adhesion numeric\n" +
                "@attribute Epith.c.size numeric\n" +
                "@attribute Bare.nuclei numeric\n" +
                "@attribute Bl.cromatin numeric\n" +
                "@attribute Normal.nucleoli numeric\n" +
                "@attribute Class {2,4}\n" +
                "\n" +
                "@data\n";
        try {
            FileWriter myWriter = new FileWriter("data/tempInstance.arff");
            myWriter.write(header);
            myWriter.write(line);
            myWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getFilename(){
        return "data/tempInstance.arff";
    }
}


