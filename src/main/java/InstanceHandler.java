import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import java.nio.file.Path;
import java.nio.file.Paths;

/** This class handles the reading of .argg files, It could potentially read .csv
 * but this functionality hasn't been tested yet.
 * Usage: Filehandler(filepath)
 */
public class InstanceHandler {
    // Set class variables
    private final String filename;
    private Instances data;

    // Instantiate
    public InstanceHandler(String filename){
        this.filename = filename;
        loadData();
    }

    /** This method read the arff file from the coresponding filename that
     * has been parsed from the constructor
     */
    private void loadData(){
        // PC in this case can be quite a bugger, this should fix the filepath so Java gets it
        Path f = Paths.get(this.filename);
        String datafile = f.toString();

        try {
            ConverterUtils.DataSource source = new ConverterUtils.DataSource(datafile);
            Instances data = source.getDataSet();
            // setting class attribute if the data format does not provide this information
            // For example, the XRFF format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
                this.data = data;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /** This method parses the read data
     * @return Instances
     */
    public Instances getInstances(){
        return this.data;
    }

}
